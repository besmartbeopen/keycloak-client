# keycloak-client

## Keycloak openapi definitions

Le definizioni openapi utilizzate sono prese da qui:

https://github.com/dahag-ag/keycloak-openapi/blob/main/OpenApiDefinitions/

## Installation & Usage

Per compilare e poi pubblicare:

```shell
./gradlew assemble
./gradlew publish
```

## Autore

Marco Andreini (at) Be Smart Be Open
